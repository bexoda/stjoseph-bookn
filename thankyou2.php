<?php

namespace Phppot;

use \Phppot\Member;

session_start();
require_once('Member.php');
$member = new Member();

$first_name =  $_SESSION['fname'];
$last_name =  $_SESSION['lname'];
$seat =  $_SESSION['seat'];

$res = $member->get_seat_number($first_name, $last_name, $seat);

$mass_num = $res[0]['mass_number'];
$full_name = $res[0]['first_name'] . " " . $res[0]['last_name'];

$seat_number = $res[0]['seat_number'];

echo "<html>" .
    "<head>" .
    "<title>Thank you for reserving your seat</title>" .
    "<link href='./css/style.css' rel='stylesheet' type='text/css' />" .
    "</head>" .
    "<body>" .
    "<div class='response-text'>" .
    "<p> Thank you for reserving your Seat!<br>" .
    "<h3><b>Name : " . $full_name .
    "</b> <br> <b>Mass: " . $mass_num .
    "</b> <br> <b> Seat Number: " . $res[0]['seat_number'] . "</b><br>" .
    "</h3></p>" .
    "</div>" .
    "</body>" .
    "</html>";

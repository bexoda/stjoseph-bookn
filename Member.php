<?php

namespace Phppot;

use \Phppot\DataSource;

class Member
{
    private $dbConn;
    private $ds;

    function __construct()
    {
        require_once 'DataSource.php';
        $this->ds = new DataSource();
    }

    function validateMember($mem)
    {
        $valid = true;
        $errorMessage = array();
        foreach ($mem as $key => $value) {
            if (empty($mem[$key])) {
                // if (empty($_POST['community2']))
                //     $valid = true;

                $valid = false;
                var_dump($_POST[$key]);
            }
        }
        if ($valid == true) {
            if ($_POST['age'] < 12) {
                $errorMessage[] = 'Age is Less than 12';
                $valid = false;
            }
        } else {
            $errorMessage[] = 'All fields are required.';
        }
        if ($valid == false) {
            return $errorMessage;
        }
        return;
    }

    function isMemberExists($first_name, $last_name, $age)
    {
        $query = 'select * FROM registered_users WHERE first_name = ? AND last_name = ? AND age = ?';
        $paramType = 'sss';
        $paramArray = array($first_name, $last_name, $age);
        $memberCount = $this->ds->numRows($query, $paramType, $paramArray);
        return $memberCount;
    }

    function insertMemberRecord($first_name, $last_name, $age, $phone, $gender, $res_address, $community, $mass_number, $seat)
    {
        $query = 'INSERT INTO registered_users (first_name, last_name, age, phone, gender, res_address, community, mass_number,seat_number) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)';
        $paramType = 'sssssssss';
        $paramArray = array($first_name, $last_name, $age, $phone, $gender, $res_address, $community, $mass_number, $seat);
        $insertId = $this->ds->insert($query, $paramType, $paramArray);
        return $insertId;
    }

    function get_reservedmembers($mass_number)
    {
        $query = 'SELECT COUNT(*) FROM registered_users WHERE mass_number = ?';
        $paramType = 's';
        $paramArray = array($mass_number);
        $insertId = $this->ds->select($query, $paramType, $paramArray);
        return $insertId;
    }

    function getSeatnumber($first_name, $last_name, $age, $mass_number)
    {
        $query = 'SELECT * FROM registered_users WHERE first_name = ? AND last_name = ?, age = ? AND mass_number = ?';
        $paramType = 'ssss';
        $paramArray = array($first_name, $last_name, $age, $mass_number);
        $memberCount = $this->ds->numRows($query, $paramType, $paramArray);
        return $memberCount;
    }


    /*  function reserve_seat($full_name)
    {
        $num = $this->get_reservedmembers();
        $numofPeople  = $num[0]['COUNT(*)'];
        $mn = ($numofPeople / 80) + 1;
        $mass_number = (int) $mn;
        $seat_number =  (80 * (($numofPeople / 80))) + 1;

        $_SESSION['fname'] = $full_name;
        $_SESSION['seat_number'] =  $seat_number;

        $query = 'INSERT INTO reserved_seats (mass_number, seat_number, full_name) VALUES (?, ?, ?)';
        $paramType = 'sss';
        $paramArray = array($mass_number, $seat_number, $full_name);
        $insertId = $this->ds->insert($query, $paramType, $paramArray);
        return $insertId;
    }*/

    function get_seat_number($first_name, $last_name, $seat_number)
    {
        $query = 'select * FROM registered_users WHERE first_name = ? AND last_name = ? AND seat_number = ?';
        $paramType = 'sss';
        $paramArray = array(
            $first_name,
            $last_name,
            $seat_number
        );
        $seat_num = $this->ds->select($query, $paramType, $paramArray);
        return $seat_num;
    }
}

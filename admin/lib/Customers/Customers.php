<?php
class Customers
{
    /**
     *
     */
    public function __construct()
    {
    }

    /**
     *
     */
    public function __destruct()
    {
    }

    /**
     * Set friendly columns\' names to order tables\' entries
     */
    public function setOrderingValues()
    {
        $ordering = [
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'gender' => 'Gender',
            'phone' => 'Phone',
            'res_address' => 'Res. Address',
            'mass_number' => 'Mass',
            'seat_number' => 'Seat'
        ];
        return $ordering;
    }
}

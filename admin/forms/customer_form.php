<script>
    function change(com) {

        var val = com.value;
        if (val === "OTHER") {
            $("#other").show();
            $("#other").attr('hidden', false);
            $("#other").removeAttr('hidden');
            $("#other").attr('hidden', false);
            $("#other").prop('hidden', false);
            //document.querySelector("#other").setAttribute("hidden", "false");

        } else {
            $("#other").hide();
            $("#other").attr('hidden', true);

            $("#other").attr('hidden', true);
            $("#other").prop('hidden', true);
        }
        return;
    }
</script>
<fieldset>
    <div class="form-group">
        <label for="f_name">First Name *</label>
        <input type="text" name="f_name" value="<?php echo htmlspecialchars($edit ? $customer['f_name'] : '', ENT_QUOTES, 'UTF-8'); ?>" placeholder="First Name" class="form-control" required="required" id="f_name">
    </div>

    <div class="form-group">
        <label for="l_name">Last name *</label>
        <input type="text" name="l_name" value="<?php echo htmlspecialchars($edit ? $customer['l_name'] : '', ENT_QUOTES, 'UTF-8'); ?>" placeholder="Last Name" class="form-control" required="required" id="l_name">
    </div>

    <div class="form-group">
        <label for="age">Age *</label>
        <input type="text" name="age" value="<?php echo htmlspecialchars($edit ? $customer['age'] : '', ENT_QUOTES, 'UTF-8'); ?>" placeholder="age" class="form-control" required="required" id="age">
    </div>

    <div class="form-group">
        <label>Gender *</label>
        <label class="radio-inline">
            <input type="radio" name="gender" value="male" <?php echo ($edit && $customer['gender'] == 'Male') ? "checked" : ""; ?> required="required" id="male" /> Male
        </label>
        <label class="radio-inline">
            <input type="radio" name="gender" value="female" <?php echo ($edit && $customer['gender'] == 'Female') ? "checked" : ""; ?> required="required" id="female" /> Female
        </label>
    </div>

    <div class="form-group">
        <label for="phone">Phone number *</label>
        <input type="text" name="phone" value="<?php echo htmlspecialchars($edit ? $customer['phone'] : '', ENT_QUOTES, 'UTF-8'); ?>" placeholder="Phone number 0241234567" class="form-control" required="required" id="phone">
    </div>

    <div class="form-group">
        <label>Community</label>
        <?php $opt_arr = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "OTHER"); ?>
        <select name="community" class="form-control selectpicker" onchange="change(this);" required>
            <option value=" ">Please select community</option>
            <?php
            foreach ($opt_arr as $opt) {
                if ($edit && $opt == $customer['community']) {
                    $sel = 'selected';
                } else {
                    $sel = '';
                }
                echo '<option value="' . $opt . '"' . $sel . '>' . $opt . '</option>';
            }
            ?>
        </select>
    </div>


    <div class="form-group" id="other" hidden="true">
        <label for="community2">Other?</label>
        <textarea name="community2" placeholder="Enter Community if other" class="form-control" id="community2" value="<?php if (isset($_POST['community2'])) echo $_POST['community2']; ?>"></textarea>
    </div>

    <div class="form-group">
        <label for="res_address">Home/Ghana Post GPS</label>
        <textarea name="res_address" placeholder="GhPost GPS/House number" class="form-control" id="res_address"><?php echo htmlspecialchars(($edit) ? $customer['res_address'] : '', ENT_QUOTES, 'UTF-8'); ?></textarea>
    </div>

    <div class="form-group">
        <label>Mass Option</label>
        <select name="mass" class="form-control selectpicker" required>
            <option Value="">Select Mass</option>
            <option Value="SATURDAY 6:30PM Mass">SATURDAY 6:30PM Mass</option>
            <option Value="SUNDAY 6:30AM Mass">SUNDAY 6:30AM Mass</option>
            <option Value="SUNDAY 8:00AM Mass">SUNDAY 8:00AM Mass</option>
            <option Value="SUNDAY 9:30AM Mass">SUNDAY 9:30AM Mass</option>
            <option Value="SUNDAY 6:30PM Mass">SUNDAY 6:30PM Mass</option>
        </select>
    </div>


    <div class="form-group text-center">
        <label></label>
        <button type="submit" class="btn btn-warning">Add Parishner <i class="glyphicon glyphicon-send"></i></button>
    </div>
</fieldset>
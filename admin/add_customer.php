<?php

namespace Phppot;

use \Phppot\Member;

require_once '../Member.php';
session_start();
$member = new Member();
require_once 'config/config.php';
require_once BASE_PATH . '/includes/auth_validate.php';

// Serve POST method, After successful insert, redirect to customers.php page.
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Mass Insert Data. Keep "name" attribute in html form same as column name in mysql table.
    $s = array_filter($_POST);
    // var_dump($_POST);

    // var_dump($s);
    $first_name = $s["f_name"];
    $_SESSION['fname']  = $first_name;
    $last_name = $s["l_name"];
    $_SESSION['lname']  = $last_name;
    $age = $s["age"];
    $phone = $s["phone"];
    $gender = $s["gender"];
    $mass =  $s["mass"];
    $res_address = $s["res_address"];
    $community = $s["community"];
    $community2 = $_POST["community2"];

    if ($community == "OTHER") {
        $$community  == $community2;
    }
    $data_to_db = array(
        "first_name" => $first_name,
        "last_name" => $last_name,
        "age" => $age,
        "phone" => $phone,
        "gender" => $gender,
        "res_address" => $res_address,
        "community" => $community,
        "mass_number" =>  $mass
    );

    // $data_to_db;

    //var_dump($data_to_db);

    $db = getDbInstance();
    $errorMessage = $member->validateMember($data_to_db);
    if (empty($errorMessage)) {
        $memberCount = $member->isMemberExists($first_name, $last_name, $age);
        if ($memberCount == 0) {
            $res =  $member->get_reservedmembers($mass);
            $numberOfreservedSeat = $res[0]['COUNT(*)'];
            if ($numberOfreservedSeat < 80) {

                $seat = $numberOfreservedSeat + 1;
                $data_to_db["seat_number"] =  $seat;
                $last_id = $db->insert('registered_users', $data_to_db);

                if ($last_id) {
                    $_SESSION['success'] = 'Parishner added successfully! Seat Number is ' . $seat;
                    // Redirect to the listing page
                    header('Location: customers.php');
                    // Important! Don't execute the rest put the exit/die.
                    exit();
                } else {
                    //   echo 'Insert failed: ' . $db->getLastError();
                    $_SESSION['failure'] = 'Insert failed: ' . $db->getLastError();;
                    // Redirect to the listing page
                    header('Location: customers.php');
                    exit();
                }
            } else {
                //  echo 'Sorry this Mass is Full';
                $_SESSION['failure'] = 'Sorry this Mass is Full';
                // Redirect to the listing page
                header('Location: customers.php');

                exit();
            }
        } else {
            // echo 'Member has Already Booked! ';
            $_SESSION['failure'] = 'Member has Already Booked! ';
            // Redirect to the listing page
            header('Location: customers.php');
            exit();
        }
    } else {
        //echo $errorMessage[0];
        $_SESSION['failure'] = $errorMessage[0];
        // Redirect to the listing page
        header('Location: customers.php');
        exit();
    }
}

// We are using same form for adding and editing. This is a create form so declare $edit = false.
$edit = false;
?>
<?php include BASE_PATH . '/includes/header.php'; ?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Add Customer</h2>
        </div>
    </div>
    <!-- Flash messages -->
    <?php include BASE_PATH . '/includes/flash_messages.php'; ?>
    <form class="form" action="" method="post" id="customer_form" enctype="multipart/form-data">
        <?php include BASE_PATH . '/forms/customer_form.php'; ?>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#customer_form').validate({
            rules: {
                f_name: {
                    required: true,
                    minlength: 3
                },
                l_name: {
                    required: true,
                    minlength: 3
                },
            }
        });
    });
</script>
<?php include BASE_PATH . '/includes/footer.php'; ?>
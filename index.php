<?php

namespace Phppot;

use \Phppot\Member;


session_start();
if (!empty($_POST["register-user"])) {

	$first_name = filter_var($_POST["fname"], FILTER_SANITIZE_STRING);
	$_SESSION['fname']  = $first_name;
	$last_name = filter_var($_POST["lname"], FILTER_SANITIZE_STRING);
	$_SESSION['lname']  = $last_name;
	$age = filter_var($_POST["age"], FILTER_SANITIZE_STRING);
	$phone = filter_var($_POST["phone"], FILTER_SANITIZE_STRING);
	$gender = filter_var($_POST["gender"], FILTER_SANITIZE_STRING);
	$mass =  filter_var($_POST["mass"], FILTER_SANITIZE_STRING);

	$res_address = filter_var($_POST["res_address"], FILTER_SANITIZE_STRING);
	$community =  filter_var($_POST["community"], FILTER_SANITIZE_STRING);
	$community2 =  filter_var($_POST["community2"], FILTER_SANITIZE_STRING);

	if ($community == "OTHER")
		$community = $community2;
	require_once("Member.php");

	$mem = array(
		"first_name" => $first_name,
		"last_name" => $last_name,
		"age" => $age,
		"phone" => $phone,
		"gender" => $gender,
		"res_address" => $res_address,
		"community" => $community,
		"mass_number" =>  $mass,

	);


	$member = new Member();
	$errorMessage = $member->validateMember($mem/*$first_name, $last_name, $age, $phone, $gender, $res_address, $mass*/);
	if (empty($errorMessage)) {
		$memberCount = $member->isMemberExists($first_name, $last_name, $age);
		if ($memberCount == 0) {
			$res = $member->get_reservedmembers($mass);
			$numberOfreservedSeat = $res[0]['COUNT(*)'];
			//var_dump($numberOfreservedSeat . " Seats");
			if ($numberOfreservedSeat < 80) {
				$_SESSION['seat']  = $numberOfreservedSeat + 1;

				$insertId = $member->insertMemberRecord($first_name, $last_name, $age, $phone, $gender, $res_address, $community, $mass, $_SESSION['seat']);
				if (!empty($insertId)) {

					header("Location: thankyou.php");
				}
			} else {
				$errorMessage[] = "Sorry this Mass is Full";
			}
		} else {
			$errorMessage[] = "You have already booked a seat";
		}
	}
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>St Joseph the Worker Seat Reservation</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<!--===============================================================================================-->
</head>

<body>


	<div class="container-contact100">
		<div class="wrap-contact100">
			<form class="contact100-form validate-form" name="frmRegistration" method="post" action="">
				<span class="contact100-form-title">
					Seat Reservation for Masses
				</span>
				<?php
				if (!empty($errorMessage) && is_array($errorMessage)) {
				?>
					<div class="error-message">
						<?php
						foreach ($errorMessage as $message) {
							echo $message . "<br/>";
						}
						?>
					</div>
				<?php
				}
				?>
				<div class="wrap-input100 validate-input" data-validate="First Name is required">
					<span class="label-input100">First Name</span>
					<input class="input100" type="text" name="fname" placeholder="Enter first your name" value="<?php if (isset($_POST['fname'])) echo $_POST['fname']; ?>">
					<span class="focus-input100"></span>
				</div>
				<div class="wrap-input100 validate-input" data-validate="Last Name is required">
					<span class="label-input100">Last Name</span>
					<input class="input100" type="text" name="lname" placeholder="Enter last your name">
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input" data-validate="Valid Age is required: 25">
					<span class="label-input100">Age</span>
					<input class="input100" type="text" name="age" placeholder="Enter your Age" value="<?php if (isset($_POST['age'])) echo $_POST['age']; ?>">
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 input100-select">
					<span class="label-input100">Gender</span>
					<div>
						<select class="selection-2" name="gender">
							<option Value="">Select gender</option>
							<option Value="Male">M</option>
							<option Value="Female">F</option>

						</select>
					</div>
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input" data-validate="Valid Phone Number is required: 0241234567">
					<span class="label-input100">Phone Number</span>
					<input class="input100" type="text" name="phone" placeholder="Enter your Phone number" value="<?php if (isset($_POST['phone'])) echo $_POST['phone']; ?>">
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 input100-select">
					<span class="label-input100">Community</span>
					<div>
						<select class="selection-2" name="community" onchange="change(this);">
							<option Value="">Select Community in which you live</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value="OTHER">OTHER</option>
						</select>
					</div>
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input" id="other" hidden="true">
					<span class="label-input100">Other</span>
					<input class="input100" type="text" name="community2" placeholder="Please Enter Community if other" value="<?php if (isset($_POST['community2'])) echo $_POST['community2']; ?>">
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input" data-validate="Valid Gh-POST GPS required: GT-123-4567">
					<span class="label-input100">Home/Ghana Post GPS</span>
					<input class="input100" type="text" name="res_address" placeholder="Please enter GhPost GPS/House number" value="<?php if (isset($_POST['res_address'])) echo $_POST['res_address']; ?>">
					<span class="focus-input100"></span>
				</div>



				<div class="wrap-input100 input100-select">
					<span class="label-input100">Mass Option</span>
					<div>
						<select class="selection-2" name="mass">
							<option Value="">Select Mass</option>
							<option Value="SATURDAY 6:30PM Mass">SATURDAY 6:30PM Mass</option>
							<option Value="SUNDAY 6:30AM Mass">SUNDAY 6:30AM Mass</option>
							<option Value="SUNDAY 8:00AM Mass">SUNDAY 8:00AM Mass</option>
							<option Value="SUNDAY 9:30AM Mass">SUNDAY 9:30AM Mass</option>
							<option Value="SUNDAY 6:30PM Mass">SUNDAY 6:30PM Mass</option>
						</select>
					</div>
					<span class="focus-input100"></span>
				</div>

				<div class="container-contact100-form-btn">
					<div class="wrap-contact100-form-btn">
						<div class="contact100-form-bgbtn"></div>
						<!-- <input type="submit" name="register-user" value="Reserve a seat" class="btnRegister"> -->
						<button type="submit" name="register-user" class="contact100-form-btn" value="Reserve a seat">
							<span>
								Reserve a Seat
								<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
							</span>
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>



	<div id="dropDownSelect1"></div>

	<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<script>
		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});


		function change(com) {

			var val = com.value;
			if (val === "OTHER") {
				$("#other").show();
				$("#other").attr('hidden', false);
				$("#other").removeAttr('hidden');
				$("#other").attr('hidden', false);
				$("#other").prop('hidden', false);
				//document.querySelector("#other").setAttribute("hidden", "false");

			}
			return;
		}
	</script>
	<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<!--===============================================================================================-->
	<script src="js/main.js"></script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-23581568-13');
	</script>

</body>
<footer>
	<p><a href="/admin">Admin?</a></p>
</footer>

</html>